#!/bin/sh

CR="
"

cat <<EoF
Test-Command: cmake -Wno-dev -S debian/tests/cmake -B \$AUTOPKGTEST_TMP
Features: test-name=cmake_find_package_CUB
Depends:
 @,
 cmake,
 g++,
Restrictions:
 superficial,

EoF

emit_cub()
{
	local cxx="$1"
	local std="${2:-17}"
	local dep="${cxx},${CR} nvidia-cuda-toolkit-gcc"
	local flaky="${CR} flaky,"
	local skippable="${CR} skippable,"
	case $cxx in
		cuda-g++)
			dep="nvidia-cuda-toolkit-gcc"
			skippable=
			;;
	esac
	cat <<EoF
Test-Command: debian/tests/compile-cub-tests ${cxx} ${std}
Features: test-name=compile_testsuite_${cxx}_C++${std}
Architecture: amd64 arm64 ppc64el
Depends:
 @,
 cmake,
 make,
 git,
 ${dep},
Restrictions:
 superficial,
 allow-stderr,${flaky}${skippable}

EoF
}

emit_cub cuda-g++ 17
emit_cub cuda-g++ 14
emit_cub g++-11 17
